# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit linux-mod-r1

VERSION="c3d1610"

DESCRIPTION="Linux kernel driver for Xbox 360 controllers with Xbox One support removed"
HOMEPAGE="https://github.com/medusalix/xpad-noone"
SRC_URI="https://github.com/medusalix/xpad-noone/archive/refs/tags/${VERSION}.tar.gz -> ${P}.tar.gz"

S="${WORKDIR}/${PN}-${VERSION}"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="${DEPEND}"

CONFIG_CHECK="INPUT_JOYDEV INPUT_JOYSTICK"
MODULES_KERNEL_MIN=4.15

src_prepare() {
	cp "${FILESDIR}/Makefile" "${S}" || die
	eapply_user
}

src_compile() {
	local modlist=(
		xpad-noone=kernel/drivers/input/joystick
	)

	linux-mod-r1_src_compile
}

src_install() {
	linux-mod-r1_src_install
}
